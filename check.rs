// expected for the below input:
// winnings_1 = 6440
// winnings_2 = 5905

const INPUT: &str = "32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";

// ---------------------------------------------------------------------------

use std::cmp::Ordering;

fn main() {
    do_part(&Hand::from_line_1);
    do_part(&Hand::from_line_2);
}

fn do_part(constructor: &dyn Fn(&str) -> Hand){
    let mut hands = build_hands(constructor);
    let winnings = compute_score(&mut hands);
    dbg!(winnings);
}

fn build_hands(constructor: &dyn Fn(&str) -> Hand) -> Vec<Hand> {
    INPUT.lines()
        .map(constructor)
        .collect()
}

const FIVE_OF_A_KIND: i32 = 7;
const FOUR_OF_A_KIND: i32 = 6;
const FULL_HOUSE: i32 = 5;
const THREE_OF_A_KIND: i32 = 4;
const TWO_PAIR: i32 = 3;
const ONE_PAIR: i32 = 2;
const HIGH_CARD: i32 = 1;

struct Hand {
    nums: Vec<i32>,
    bid: i32,
    value: i32,
}

impl Hand {

    fn from_line_1(line: &str) -> Hand {
        let (cards, bid) = line.split_once(" ").unwrap();
        let nums = parse_cards(cards, "23456789TJQKA");
        let value = compute_value_1(&nums);
        Hand {
            nums: nums,
            bid: bid.parse().unwrap(),
            value: value,
        }
    }
    
    fn from_line_2(line: &str) -> Hand {
        let (cards, bid) = line.split_once(" ").unwrap();
        let nums = parse_cards(cards, "J23456789TQKA");
        let value = compute_value_2(&nums, 0);
        Hand {
            nums: nums,
            bid: bid.parse().unwrap(),
            value: value,
        }
    }
    
}

fn compute_score(hands: &mut Vec<Hand>) -> usize {
    hands.sort_unstable_by(compare_hands);
    compute_winnings(hands)
}

fn compare_hands(first: &Hand, second: &Hand) -> Ordering {
    if first.value != second.value {
        return first.value.cmp(&second.value);
    }
    for (a, b) in first.nums.iter().zip(second.nums.iter()) {
        if a != b {
            return a.cmp(b);
        }
    }
    return Ordering::Equal;  
}

fn compute_winnings(hands: &Vec<Hand>) -> usize {
    hands.iter()
        .enumerate()
        .map(|(i, h)| (i + 1) * (h.bid as usize))
        .sum()
}

fn parse_cards(cards: &str, map: &str) -> Vec<i32> {
    cards.chars()
        .map(|c| card_to_int(c, map).unwrap())
        .collect()
}

fn card_to_int(c: char, map: &str) -> Option<i32> {
    for (i, e) in map.char_indices() {
        if e == c {
            return Some(i as i32);
        }
    }
    return None;
}

fn build_counter(vec: &Vec<i32>) -> Vec<i32> {
    let mut counter = [0; 13];
    for k in vec {
        counter[*k as usize] += 1;
    }
    counter.iter()
        .filter_map(|x| if *x == 0 {None} else {Some(*x)})
        .collect()
}

fn compute_value_1(nums: &Vec<i32>) -> i32 {
    let values = build_counter(nums);
    let len = values.len();
    match len {
        1 => FIVE_OF_A_KIND,
        2 => {
            if values.contains(&4) {
                FOUR_OF_A_KIND
            } else {
                FULL_HOUSE
            }
        }
        3 => {
            if values.contains(&3) {
                THREE_OF_A_KIND
            } else {
                TWO_PAIR
            }
        }
        4 => ONE_PAIR,
        _ => HIGH_CARD,
    }
}

fn compute_value_2(nums: &Vec<i32>, j_val: i32) -> i32 {
    let mut nj = 0;
    let mut other = Vec::new();
    for n in nums {
        if *n == j_val {
            nj += 1;
        } else {
            other.push(n.clone());
        }
    }
    let other_cards = build_counter(&other);
    let n_others = other_cards.len();
    match nj {
        4 | 5 => FIVE_OF_A_KIND,
        3 => {
            if n_others == 1 {
                FIVE_OF_A_KIND
            } else {
                FOUR_OF_A_KIND
            }
        }
        2 => match n_others {
            1 => FIVE_OF_A_KIND,
            2 => FOUR_OF_A_KIND,
            _ => THREE_OF_A_KIND,
        },
        1 => match n_others {
            1 => FIVE_OF_A_KIND,
            2 => {
                if other_cards.contains(&3) {
                    FOUR_OF_A_KIND
                } else {
                    FULL_HOUSE
                }
            }
            3 => THREE_OF_A_KIND,
            _ => ONE_PAIR,
        },
        _ => compute_value_1(&nums),
    }
}
