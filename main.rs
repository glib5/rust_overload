#[allow(unused)]
use std::cmp::Ordering;
use std::collections::HashMap;

mod utils;

fn main() {
    // --------------------------------------------------
    // let input = utils::read_input(true);
    // --------------------------------------------------
    let is_test = true;
    part1(is_test.clone());
    part2(is_test.clone());
}

const FIVE_OF_A_KIND: i32 = 7;
const FOUR_OF_A_KIND: i32 = 6;
const FULL_HOUSE: i32 = 5;
const THREE_OF_A_KIND: i32 = 4;
const TWO_PAIR: i32 = 3;
const ONE_PAIR: i32 = 2;
const HIGH_CARD: i32 = 1;

fn part1(is_test: bool) -> () {
    println!("part1");
    let input = utils::read_input(is_test);
    const CARDS1: &str = "23456789TJQKA";
    let mut hands = input
        .lines()
        .map(|line| {
            let (cards, bid) = line.split_once(" ").unwrap();
            let nums = parse_cards(cards, CARDS1);
            let value = compute_value_1(&nums);
            let numeric_bid = bid.parse().unwrap();
            Hand {
                nums: nums,
                bid: numeric_bid,
                value: value,
            }
        })
        .collect::<Vec<Hand>>();
    hands.sort_unstable();
    let winnings = compute_winnings(hands);
    dbg!(winnings);
}

fn compute_winnings(hands: Vec<Hand>) -> usize {
    hands
        .iter()
        .enumerate()
        .map(|(i, h)| (i + 1) * (h.bid as usize))
        .sum()
}

fn part2(is_test: bool) -> () {
    println!("part2");
    let input = utils::read_input(is_test);
    const CARDS2: &str = "J23456789TQKA";
    let mut hands = input
        .lines()
        .map(|line| {
            let (cards, bid) = line.split_once(" ").unwrap();
            let nums = parse_cards(cards, CARDS2);
            let value = compute_value_2(&nums, 0);
            let numeric_bid = bid.parse().unwrap();
            Hand {
                nums: nums,
                bid: numeric_bid,
                value: value,
            }
        })
        .collect::<Vec<Hand>>();
    hands.sort_unstable();
    let winnings = compute_winnings(hands);
    dbg!(winnings);
}

fn compute_value_1(nums: &Vec<i32>) -> i32 {
    let values = build_counter(nums);
    let len = values.len();
    match len {
        1 => FIVE_OF_A_KIND,
        2 => {
            if values.contains(&4) {
                FOUR_OF_A_KIND
            } else {
                FULL_HOUSE
            }
        }
        3 => {
            if values.contains(&3) {
                THREE_OF_A_KIND
            } else {
                TWO_PAIR
            }
        }
        4 => ONE_PAIR,
        _ => HIGH_CARD,
    }
}

fn compute_value_2(nums: &Vec<i32>, j_val: i32) -> i32 {
    let mut nj = 0;
    let mut other = Vec::new();
    for n in nums {
        if *n == j_val {
            nj += 1;
        } else {
            other.push(n.clone());
        }
    }
    let other_cards = build_counter(&other);
    let n_others = other_cards.len();
    match nj {
        4 | 5 => FIVE_OF_A_KIND,
        3 => {
            if n_others == 1 {
                FIVE_OF_A_KIND
            } else {
                FOUR_OF_A_KIND
            }
        }
        2 => match n_others {
            1 => FIVE_OF_A_KIND,
            2 => FOUR_OF_A_KIND,
            _ => THREE_OF_A_KIND,
        },
        1 => match n_others {
            1 => FIVE_OF_A_KIND,
            2 => {
                if other_cards.contains(&3) {
                    FOUR_OF_A_KIND
                } else {
                    FULL_HOUSE
                }
            }
            3 => THREE_OF_A_KIND,
            _ => ONE_PAIR,
        },
        _ => compute_value_1(&nums),
    }
}

fn parse_cards(cards: &str, map: &str) -> Vec<i32> {
    cards
        .chars()
        .map(|c| card_to_int(c, map).unwrap())
        .collect::<Vec<i32>>()
}

fn card_to_int(c: char, map: &str) -> Option<i32> {
    for (i, e) in map.char_indices() {
        if e == c {
            return Some(i as i32);
        }
    }
    return None;
}

fn build_counter(v: &Vec<i32>) -> Vec<i32> {
    let mut counter = HashMap::with_capacity(5);
    for i in v {
        counter.entry(i).and_modify(|n| *n += 1).or_insert(1);
    }
    counter.values().map(|x| x.clone()).collect::<Vec<i32>>()
}

struct Hand {
    nums: Vec<i32>,
    bid: i32,
    value: i32,
}

impl Eq for Hand {}

impl PartialEq for Hand {
    fn eq(&self, other: &Self) -> bool {
        if self.value != other.value {
            return false;
        }
        return self.nums == other.nums;
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.value != other.value {
            return self.value.cmp(&other.value);
        }
        for (a, b) in self.nums.iter().zip(other.nums.iter()) {
            if a == b {
                continue;
            }
            return a.cmp(b);
        }
        return Ordering::Equal;
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

// ----------------------------------------------------------------------------------------------

// the one below seems the better option, especially if you 
// just need to sort, usually once or twice max


const N: usize = 3;

fn main() {
    // set up vector of non-trivial types
    let mut arr = Vec::with_capacity(N);
    for i in 0..N {
        arr.push(Foo::from_int(i));
    }

    // for a base case
    arr.sort_unstable_by_key(|a| a.baz);

    // for more complex comparison logic
    arr.sort_unstable_by(|a, b| a.baz.cmp(&b.baz));
}

struct Foo {
    bar: usize,
    baz: usize,
}

impl Foo {
    fn from_int(i: usize) -> Foo {
        Foo {
            bar: i,
            baz: (N * 2) - i,
        }
    }
}


